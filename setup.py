from setuptools import setup

setup(name='whois_domreg_lt',
      version='0.1',
      description='LT domreg whois based on sockets',
      url='https://gitlab.com/kazys-cernauskis/whois_domreg_ltt',
      author='blog.crn.lt',
      author_email='kazys@crn.lt',
      license='MIT',
      packages=['whois_domreg_lt'],
      zip_safe=True)