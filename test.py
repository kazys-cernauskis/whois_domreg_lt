#!/usr/bin/env python
'''
domreg test script
'''
from pprint import pprint
import whois_domreg_lt


TLD = whois_domreg_lt.Whois()

pprint(TLD.get('x8.lt', False))
print('---')
print(TLD.get('aaa.lt', False))
print('---')
print(TLD.get('x8.lt', True))
print('---')
print(TLD.get('aaa.lt', True))
