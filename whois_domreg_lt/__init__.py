'''
domreg whois raw socket module
with minimal status response
and full whois response
'''
import socket

class Whois:
    '''
    whois class
    '''
    def __init__(self):
        '''
        whois constructor
        '''
        self.whois_addr = 'whois.domreg.lt'
        self.das_addr = 'das.domreg.lt'
        self.whois_port = 43
        self.das_port = 4343

    def __open_socket(self, dst):
        '''
        whois open socket method
        '''
        if dst == 'das':
            tcp_port = self.das_port
            domreg_ip = socket.gethostbyname(self.das_addr)
        else:
            tcp_port = self.whois_port
            domreg_ip = socket.gethostbyname(self.whois_addr)
        tcp_ip = domreg_ip
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((tcp_ip, tcp_port))

    def __recv(self):
        '''
        whois response receive method
        '''
        total_data = []
        while True:
            data = self.sock.recv(8192)
            if not data:
                break
            total_data.append(data)
        return b''.join(total_data).decode("utf-8")

    def __send(self, msg):
        '''
        whois request send method
        '''
        self.sock.send(msg)

    def get(self, domain, full=False):
        '''
        whois main public function to query whois
        '''
        if full:
            self.__open_socket('whois')
            msg = "{}\r\n".format(domain).encode()
        else:
            self.__open_socket('das')
            msg = "get 1.0 {}\r\n".format(domain).encode()
        self.__send(msg)
        info = self.__recv()
        return self.__parse(info)

    def __parse(self, data):
        '''
        whois response parser
        '''
        res = {}
        name_servers = []
        for i in data.splitlines():
            if not i.startswith('%'):
                if "\t" in i:
                    line = i.split("\t")
                else:
                    line = i.split()
                if len(line) > 2:
                    for idx in range(1, len(line)):
                        if line[idx] != "":
                            if line[0] == "Nameserver:":
                                name_servers.append(line[idx])
                            else:
                                res[line[0][:-1].replace(" ", "_")] = line[idx]
                else:
                    res[line[0][:-1].replace(" ", "_")] = line[1]
        if len(name_servers) > 0:
            res['ns'] = name_servers
        return res
